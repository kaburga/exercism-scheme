(import (rnrs))

(define (armstrong-number? n)
  (let* ([digit-list (map char->digit (string->list
                                   (number->string n)))]
         [number-of-digits (length digit-list)]
         [armstrong (fold-left (lambda (accumulator digit) (+ accumulator
                                                              (expt digit number-of-digits)))
                               0
                               digit-list)])
    (equal? n armstrong)))

(define (char->digit ch)
  (cond
   [(char-ci=? #\0 ch) 0]
   [(char-ci=? #\1 ch) 1]
   [(char-ci=? #\2 ch) 2]
   [(char-ci=? #\3 ch) 3]
   [(char-ci=? #\4 ch) 4]
   [(char-ci=? #\5 ch) 5]
   [(char-ci=? #\6 ch) 6]
   [(char-ci=? #\7 ch) 7]
   [(char-ci=? #\8 ch) 8]
   [(char-ci=? #\9 ch) 9]))
