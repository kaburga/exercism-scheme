(import (rnrs))

(define (square n)
  (if (or (< n 1) (> n 64))
      (error "n out of bounds")
      (expt 2 (sub1 n))))

(define total
  (sub1 (* 2 (square 64))))
