(import (rnrs))

(define (word-sorted-by-characters word)
  (sort char<?
        (string->list (string-downcase word))))

(define (anagram target words)
  (filter (lambda (word)
            (and (not (equal? (string-downcase target)
                              (string-downcase word)))
                 (equal? (word-sorted-by-characters target)
                         (word-sorted-by-characters word))))
          words))
