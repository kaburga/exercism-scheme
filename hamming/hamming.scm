(import (rnrs))

(define (hamming-distance strand-a strand-b)
  (let ((strand-a-list (string->list strand-a))
        (strand-b-list (string->list strand-b)))
    (length (delq #f (map (lambda (x y) (not (char=? x y)))
                          strand-a-list
                          strand-b-list)))))
