(import (rnrs))

(define (nucleotide-count dna)
  (let ([dna-list (string->list dna)]
        [a 0]
        [c 0]
        [g 0]
        [t 0])
    (for-each (lambda (nucl) (cond
                              [(char-ci=? #\A nucl) (set! a (add1 a))]
                              [(char-ci=? #\C nucl) (set! c (add1 c))]
                              [(char-ci=? #\G nucl) (set! g (add1 g))]
                              [(char-ci=? #\T nucl)   (set! t (add1 t))]
                              [else (error "not a nucleotide")]))
              dna-list)
    (list (cons #\A a) (cons #\C c) (cons #\G g) (cons #\T t))))
