(import (rnrs))

(define (convert-nucleotide nuc)
  (case nuc
    ((#\A) #\U)
    ((#\C) #\G)
    ((#\G) #\C)
    ((#\T) #\A)))

(define (dna->rna dna-string)
  (let ((dna-list (string->list dna-string)))
    (list->string (map convert-nucleotide dna-list))))
